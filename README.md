# proyecto_sxe

## Gestión Productos

 En esta aplicación nace de la necesidad de controlar las diferentes compras de productos para una tienda. Vamos a gestionarla de tal forma que vamos a controlar los distribuidores de los prodcutos y a su vez cada vez que se hace una compra de ese producto, se va a guardar el valor total de la compra y la cantidad de la compra y a partir de estes campos, se va a calcular el precio de la unidad. Este calculo nos va a ayudar a comprobar cual es el distribuidor con el que tenemos mejor precio. Las comprar las vamos a poder ver en el tiempo a traves e un calendario.

 Esta aplicación en un futuro, se le añadiran funcionalidades como organizar las compras por los distribuidores o gestionar compras automaticas, o establecer el precio recomendado del producto segun las ganancias deseadas.
