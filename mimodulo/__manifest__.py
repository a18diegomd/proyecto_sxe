# -*- coding: utf-8 -*-
{
    'name': "Mi modulo",  # Module title
    'summary': "Gestionar productos",  # Module subtitle phrase
    'description': """Esta aplicación esta diseñada para la gestión de los productos y de sus distribuidores""",  # You can also rst format
    'author': "Diego Martínez de Santiago",
    #'website': "http://www.example.com",
    'category': 'Uncategorized',
    'version': '12.0.1',
    'depends': ['base'],
    # This data files will be loaded at the installation (commented becaues file is not added in this example)
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/productos.xml',
        'views/distribuidores.xml',
        'views/compras.xml',
    ],
    # This demo data files will be loaded if db initialize with demo data (commented becaues file is not added in this example)
    # 'demo': [
    #     'demo.xml'
    # ],
}
