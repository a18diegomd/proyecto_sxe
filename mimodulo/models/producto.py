# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, api
from odoo.exceptions import UserError
from odoo.tools.translate import _

from datetime import datetime, timedelta


logger = logging.getLogger(__name__)


class Producto(models.Model):
    _name = 'producto.producto'
    _description = 'Producto'
    name = fields.Char('Nombre', required=True)
    descriptionpro = fields.Char('Descripcion')

    date_release = fields.Date('Fecha de la primera compra')
    state = fields.Selection([
        ('unavailable', 'No Disponible'),
        ('available', 'Disponible'),
        ],
        'State', default="available")


    compra_ids = fields.One2many('producto.compras', inverse_name='producto_id')

    producto_imagen = fields.Binary('Portada')
        
    @api.model
    def is_allowed_transition(self, old_state, new_state):
        allowed = [
                   ('available', 'unavailable'),
                   ('unavailable', 'available'),
        ]
        return (old_state, new_state) in allowed

    @api.multi
    def change_state(self, new_state):
        for producto in self:
            if producto.is_allowed_transition(producto.state, new_state):
                producto.state = new_state
            else:
                message = _('Moving from %s to %s is not allowd') % (producto.state, new_state)
                raise UserError(message)

    def make_available(self):
        self.change_state('available')

    def make_unavailable(self):
        self.change_state('unavailable')
    

    @api.multi
    def change_update_date(self):
        self.ensure_one()
        self.date_updated = fields.Datetime.now()

   
class ProductoCompras(models.Model):
    _name = 'producto.compras'
    _description = 'Compras'
    _order = 'date_start desc'
    _inherits = {'producto.producto': 'producto_id'}
    

    @api.one
    @api.depends('importetotal')
    def calc(self):
        if self.importetotal:
            self.importeunidad = self.importetotal / self.cantidad
        return

    distribuidor_id = fields.Many2one('distribuidor.distribuidor', required=True)
    producto_id =  fields.Many2one('producto.producto', ondelete='cascade',required=False)
    date_start = fields.Date('Fecha Distribución', default=lambda *a:datetime.now().strftime('%Y-%m-%d'))
    date_end = fields.Date('Compra End', default=lambda *a:(datetime.now() + timedelta(days=(6))).strftime('%Y-%m-%d'))
    cantidad = fields.Integer('Cantidad', required=True)
    importetotal = fields.Float('Importe Total', required=True)
    importeunidad = fields.Float('Importe Unidad',required=False, compute=calc)


    @api.constrains('date_end', 'date_start')
    def _check_dates(self):
        for compra in self:
            if compra.date_start > compra.date_end:
                raise models.ValidationError('Start date After end date!')


