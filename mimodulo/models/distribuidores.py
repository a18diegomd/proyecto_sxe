# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class Distribuidores(models.Model):
    _name = 'distribuidor.distribuidor'

    _parent_store = True
    _parent_name = "parent_id"  # optional if field is 'parent_id'

    name = fields.Char('Nombre')
    description = fields.Text('Descripción')

    parent_id = fields.Many2one(
        'distribuidor.distribuidor',
        string='Parent Category',
        ondelete='restrict',
        index=True
    )

    parent_path = fields.Char(index=True)

    @api.constrains('parent_id')
    def _check_hierarchy(self):
        if not self._check_recursion():
            raise models.ValidationError('Error! You cannot create recursive categories.')